module.exports = {
  // Homogeneous CRUD
  country: "country",
  environmentType: "environmentType",

  // Hetrogeneous
  providerCategory: "providerCategory",

  countryProviders: "countryProviders",
  categoryProviders: "categoryProviders",

  providerApiProduct: "providerApiProduct",
  providerProducts: "providerProducts",

  productMethods: "productMethods",

  methodFields: "methodFields",

  fieldValues: "fieldValues",

  providerProductMethodToExecute: "providerProductMethodToExecute",
};
