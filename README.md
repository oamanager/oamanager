## `The official website to register and start working will be announced on 20th June 2021`

[![npm](https://img.shields.io/npm/v/oamanager)](https://npm.im/oamanager) ![Supports Android, iOS, macOS, and Windows](https://img.shields.io/badge/platforms-android%20|%20ios%20|%20macos%20|%20windows-orange.svg) ![NPM](https://img.shields.io/npm/l/oamanager)
![npm](https://img.shields.io/npm/dm/oamanager)

Open API Manager APIs for Android, iOS, macOS & Windows. It allows you to get information on:

- Get Providers by country
- Get Providers by their category like Financial, TPP etc
- Get API Products of any Provider
- Get Methods of any API Product
- Get Fields of any API Product Method
- Get Possible Values of any API Product Method Field

## Getting started

Install the library using either Yarn:

```
npm install --save oamanager
```

or yarn:

```
yarn add oamanager
```

## Prerequisite

1. Get register yourself on `{Wait needed till 20 Jun}` .
2. Get your Authorization-Key and Communication-Key by using `{Wait needed till 20 Jun}/profile`.

### How to use?

- Import the package and call the api

```javascript
import OA from "oamanager";

OA.getCountryList(
  "Bearer --Your authorization key--",
  "Basic --Your communication key--"
)
  .then((data) => console.log(data))
  .catch((error) => console.error(error));
```

- API will give similar response

```json
[
  {
    "code": "2b1239oGBOZLmmmS55RVEMxPluYR8ZCZNywpPKf8Jxp2ZYaHNwdDUcm",
    "name": "Sweden",
    "alpha2": "SE"
  },
  ...
]
```

##### Parameter required for all api calls

| Parameter  | Type                                | Description                                                                       |
| ---------- | ----------------------------------- | --------------------------------------------------------------------------------- |
| `Auth-Key` | `Bearer --Your authorization key--` | You can get your authorization-key from your profile at {Wait needed till 20 Jun} |
| `Comm-Key` | `Basic --Your communication key--`  | You can get your communicatio-key from your profile at {Wait needed till 20 Jun}  |

### List of API calls available

1. getCountryList (auth, key)
1. getEnvironmentTypesList (auth, key)
1. getProvidersCategoryList (auth, key)
1. getProvidersListByCountry (auth, key, code)
1. getProvidersListByCategory (auth, key, code)
1. getProductsListByProvider (auth, key, code)
1. getMethodsListByProduct (auth, key, code)
1. getMethodsListByProductAndEnvironment (auth, key, productCode, envTypeCode)
1. getFieldsListByMethod (auth, key, code)

#### 1. getCountryList (auth, key)

This call will return the list of available countries

- Import the package and call the api

```javascript
import OA from "oamanager";

OA.getCountryList(
  "Bearer --Your authorization key--",
  "Basic --Your communication key--"
)
  .then((data) => console.log(data))
  .catch((error) => console.error(error));
```

- API will give similar response

```json
[
  {
    "code": "2b1239oGBOZLmmmS55RVEMxPluYR8ZCZNywpPKf8Jxp2ZYaHNwdDUcm",
    "name": "Sweden",
    "alpha2": "SE"
  },
  ...
]
```

#### 2. getEnvironmentTypesList (auth, key)

This call will return the list of available environment types

- Import the package and call the api

```javascript
import OA from "oamanager";

OA.getEnvironmentTypesList(
  "Bearer --Your authorization key--",
  "Basic --Your communication key--"
)
  .then((data) => console.log(data))
  .catch((error) => console.error(error));
```

- API will give similar response

```json
[
  {
    "code": "2b12BjJiafmBVLXHGmwNd9O8HSyJGx7JkLYC1fzl4s7XZcIsTycZbO",
    "name": "SandBox"
  },
  ...
]
```

#### 3. getProvidersCategoryList (auth, key)

This call will return the list of available provider categories

- Import the package and call the api

```javascript
import OA from "oamanager";

OA.getProvidersCategoryList(
  "Bearer --Your authorization key--",
  "Basic --Your communication key--"
)
  .then((data) => console.log(data))
  .catch((error) => console.error(error));
```

- API will give similar response

```json
[
  {
    "code": "2b12SCm7t6RA2Akm4k6KhxohuYUs8yaCmtLOzsM6DAhO9ueVhGsXbb9uwe89",
    "name": "Financial"
  },
  ...
]
```


#### 4. getProvidersListByCountry (auth, key, code)

This call will return the list of all providers associated with country

- Import the package and call the api

```javascript
import OA from "oamanager";

OA.getProvidersListByCountry(
  "Bearer --Your authorization key--",
  "Basic --Your communication key--",
  "2b1239oGBOZLmmmS55RVEMxPluYR8ZCZNywpPKf8Jxp2ZYaHNwdDUcm"
)
  .then((data) => console.log(data))
  .catch((error) => console.error(error));
```

- API will give similar response

```json
[
  {
     "code": "2b12tUkti4TiWt4QqwSYT244dukKinxPAZ1S1Iyyynhzi4chrz0eJuvC",
     "countryId": 1,
     "userId": 1,
     "providerCategoryId": 1,
     "provider_name": "SEB",
     "protocol": "https",
     "base_url": "api-sandbox.sebgroup.com",
     "comment": "This is SEB bank with it's base url"
  },
  ...
]
```

- Parameter required

| Parameter  | Type                                | Description                                                                              |
| ---------- | ----------------------------------- | ---------------------------------------------------------------------------------------- |
| `Code`     | `STRING`                            | Code of any country from the list received in response of getCountryList call            |

#### 5. getProvidersListByCategory (auth, key, code)

This call will return the list of all providers associated with provider category

- Import the package and call the api

```javascript
import OA from "oamanager";

OA.getProvidersListByCountry(
  "Bearer --Your authorization key--",
  "Basic --Your communication key--",
  "2b12SCm7t6RA2Akm4k6KhxohuYUs8yaCmtLOzsM6DAhO9ueVhGsXbb9uwe89"
)
  .then((data) => console.log(data))
  .catch((error) => console.error(error));
```

- API will give similar response

```json
[
  {
     "code": "2b12tUkti4TiWt4QqwSYT244dukKinxPAZ1S1Iyyynhzi4chrz0eJuvC",
     "countryId": 1,
     "userId": 1,
     "providerCategoryId": 1,
     "provider_name": "SEB",
     "protocol": "https",
     "base_url": "api-sandbox.sebgroup.com",
     "comment": "This is SEB bank with it's base url"
  },
  ...
]
```

- Parameter required

| Parameter  | Type                                | Description                                                                              |
| ---------- | ----------------------------------- | ---------------------------------------------------------------------------------------- |
| `Code`     | `STRING`                            | Code of any provider category from the list received in response of getProvidersCategoryList call  |

#### 6. getProductsListByProvider (auth, key, code)

This call will return the list of all provider products associated with provider

- Import the package and call the api

```javascript
import OA from "oamanager";

OA.getProductsListByProvider(
  "Bearer --Your authorization key--",
  "Basic --Your communication key--",
  "2b12tUkti4TiWt4QqwSYT244dukKinxPAZ1S1Iyyynhzi4chrz0eJuvC"
)
  .then((data) => console.log(data))
  .catch((error) => console.error(error));
```

- API will give similar response

```json
[
  {
    "code": "2b12fYGpXfwYfH0uBX0yhvitYhhDYOrPLrz7aRiqmYu6kzLQ3EL6PY6",
    "providerId": 2,
    "providerApiProductId": null,
    "name": "Authorization",
    "route": "/mga/sps/oauth/oauth20",
    "version": "v1"
  },
  ...
]
```

- Parameter required

| Parameter  | Type                                | Description                                                                              |
| ---------- | ----------------------------------- | ---------------------------------------------------------------------------------------- |
| `Code`     | `STRING`                            | Code of any provider  from the list received in response of getProvidersListByCountry or getProvidersListByCategory call  |

#### 7. getMethodsListByProduct (auth, key, code)

This call will return the list of all methods associated with provider product

- Import the package and call the api

```javascript
import OA from "oamanager";

OA.getMethodsListByProduct(
  "Bearer --Your authorization key--",
  "Basic --Your communication key--",
  "2b12fYGpXfwYfH0uBX0yhvitYhhDYOrPLrz7aRiqmYu6kzLQ3EL6PY6"
)
  .then((data) => console.log(data))
  .catch((error) => console.error(error));
```

- API will give similar response

```json
[
  {
    "code": "2b12oJzZbw3wdQFlFWfvb3gySuddALGMhTBcYvnEeDssc0QFEisJGGvG",
    "name": "Get Authorize",
    "status": true,
    "baseUrl": null,
    "route": "/authorize",
    "comment": "This is SEB method to get authorize",
    "preScript": "",
    "postScript": "",
    "obaServiceRoute": "/obaAuthorize",
    "methodTypeId": 1,
    "enviornmentTypeId": 1,
    "enviornmentAvailabilityId": 1,
    "providerApiProductId": 1
  },
  ...
]
```

- Parameter required

| Parameter  | Type                                | Description                                                                              |
| ---------- | ----------------------------------- | ---------------------------------------------------------------------------------------- |
| `Code`     | `STRING`                            | Code of any provider product  from the list received in response of getProductsListByProvider call  |

#### 8. getMethodsListByProductAndEnvironment (auth, key, productCode, envTypeCode)

This call will return the list of all methods associated with provider product and environment type

- Import the package and call the api

```javascript
import OA from "oamanager";

OA.getMethodsListByProductAndEnvironment(
  "Bearer --Your authorization key--",
  "Basic --Your communication key--",
  "2b12fYGpXfwYfH0uBX0yhvitYhhDYOrPLrz7aRiqmYu6kzLQ3EL6PY6"
  "2b12BjJiafmBVLXHGmwNd9O8HSyJGx7JkLYC1fzl4s7XZcIsTycZbO"
)
  .then((data) => console.log(data))
  .catch((error) => console.error(error));
```

- API will give similar response

```json
[
  {
    "code": "2b12oJzZbw3wdQFlFWfvb3gySuddALGMhTBcYvnEeDssc0QFEisJGGvG",
    "name": "Get Authorize",
    "status": true,
    "baseUrl": null,
    "route": "/authorize",
    "comment": "This is SEB method to get authorize",
    "preScript": "",
    "postScript": "",
    "obaServiceRoute": "/obaAuthorize",
    "methodTypeId": 1,
    "enviornmentTypeId": 1,
    "enviornmentAvailabilityId": 1,
    "providerApiProductId": 1
  },
  ...
]
```

- Parameter required

| Parameter       | Type                                | Description                                                                                         |
| --------------- | ----------------------------------- | --------------------------------------------------------------------------------------------------- |
| `Product Code`  | `STRING`                            | Code of any provider product  from the list received in response of getProductsListByProvider call  |
| `Env Code`      | `STRING`                            | Code of any environment type from the list received in response of getEnvironmentTypesList call     |



#### 9. getFieldsListByMethod (auth, key, code)

This call will return the list of all fields with their possible values associated with product methods

- Import the package and call the api

```javascript
import OA from "oamanager";

OA.getFieldsListByMethod(
  "Bearer --Your authorization key--",
  "Basic --Your communication key--",
  "2b12oJzZbw3wdQFlFWfvb3gySuddALGMhTBcYvnEeDssc0QFEisJGGvG"
)
  .then((data) => console.log(data))
  .catch((error) => console.error(error));
```

- API will give similar response

```json
[
  {
    "code": "2b12tlgFqAJXLZTocFhDgw06eVKfKxMFg7m30GKEINClEFs57qdgTlRq",
    "title": "Accept",
    "key": "accept",
    "comment": null,
    "isRequired": true,
    "childOf": null,
    "FieldPossibleValues": [
      {
        "code": "2b12spN9EuVaqIJILzLDrS3iIuYJOJxkhUzA9h7LSz2m4ttU8nPYufh",
        "value": "application/json"
      }
    ]
  },
  ...
]
```

- Parameter required

| Parameter  | Type                                | Description                                                                              |
| ---------- | ----------------------------------- | ---------------------------------------------------------------------------------------- |
| `Code`     | `STRING`                            | Code of any product method from the list received in response of getMethodsListByProduct or getMethodsListByProductAndEnvironment call  |
